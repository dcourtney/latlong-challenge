import React, { Component } from 'react';
import logo from './barcode.png'; 
import "antd/dist/antd.css";
import { Row, Col } from 'antd';
import './App.css';


class BarcodeForm extends Component {
  constructor(props){
    super(props);
    //Initial values for the state
    this.state = {
      value: '',
      isLoading: false,
      error: null,
      items: []
    };
    //Bindings for the handleChange and handleSubmit functions
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event){
    this.setState({value: event.target.value});
  }

  handleSubmit(event){
    const barcode = this.state.value;
    const url = `https://world.openfoodfacts.org/api/v0/product/${barcode}.json`;
    //error handling for invalide barcodes
    if (!barcode) {
      return alert("Please enter a valid barcode!");
    } else if( isNaN(barcode)){
      return alert("Please enter a valid barcode!");
    }
    event.preventDefault();
    this.fetchOpenFoodApi(url);
  }

fetchOpenFoodApi(url) {
  // Where we're fetching data from
  fetch(url)
    // We get the API response and receive data in JSON format...
    .then(res => res.json())
    .then(
      (result) => {
        console.log(result);
        this.setState({
          isLoaded: true,
          items: result.product.ingredients
        })
      })
    .catch(error => alert("Please enter a valid barcode!")); 
      
    }
  
  render() {
    const { items } = this.state;
      return (
        <div id = "barcodeForm">
          <Row id = "header">
            <Col span={24}><h1 id="title">What's in my food?</h1></Col>
          </Row>
          <Row id = "barcodeImg">
            <Col span={8}></Col>
            <Col span={8}><img src = {logo} alt = "barcode"></img></Col>
            <Col span={8}></Col>
          </Row>
          <Row>
            <Col span={8}></Col>
            <Col span={8}>
              <form onSubmit={this.handleSubmit}> 
                  <label>
                    <input type="text" placeholder="Enter Barcode Here" value={this.state.value} onChange={this.handleChange}/>
                  </label>
                  <input id="submitBtn" type="submit" name="Submit"></input>
              </form></Col>
            <Col span={8}></Col>
          </Row>
          <Row id = "ingredientList">
            <Col span={8}></Col>
            <Col span={8}>
              <ul>
                {items.map(item => (
                  <li key={item.text}>
                    {item.text} 
                  </li>
                ))}
              </ul></Col>
            <Col span={8}></Col>
          </Row>
        </div>
      );
    }
  }

export default BarcodeForm;
